package com.homelab.slideshow.model;

public class ImageDataItem {

    private int id;
    private int number;
    private String url;
    private boolean isFavorites;
    private String note;

    public ImageDataItem(){}

    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }

    public int getNumber(){
        return number;
    }
    public void setNumber(int number){
        this.number = number;
    }

    public String getUrl(){
        return url;
    }
    public void setUrl(String url){
        this.url = url;
    }

    public boolean getIsFavorites(){
        return isFavorites;
    }
    public void setIsFavorites(boolean isFavorites){
        this.isFavorites = isFavorites;
    }

    public String getNote(){
        return note;
    }
    public void setNote(String note){
        this.note = note;
    }

    public String getNumberString(){
        return number + "";
    }
}
