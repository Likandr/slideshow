package com.homelab.slideshow.model;

import java.util.ArrayList;

public class ImageDataList {

    private ArrayList<ImageDataItem> imageDataList = new ArrayList<>();

    public ImageDataList(){

    }

    public void add(ImageDataItem imageDataItem){
        this.imageDataList.add(imageDataItem);
    }

    public ArrayList<ImageDataItem> getImageDataList(){
        return imageDataList;
    }

    public void setImageDataList(ArrayList<ImageDataItem> imageDataList){
        this.imageDataList = imageDataList;
    }

    public int length(){
        return imageDataList.size();
    }

    public ImageDataItem getImageData(int id){
        return imageDataList.get((imageDataList.size() != id) ? id : 1);
    }

    public boolean isEmpty(){
        return (imageDataList.size() == 1);
    }
}
