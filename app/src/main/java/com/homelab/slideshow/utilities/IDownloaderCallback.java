package com.homelab.slideshow.utilities;

public interface IDownloaderCallback<V> {
    void onDone();
}
