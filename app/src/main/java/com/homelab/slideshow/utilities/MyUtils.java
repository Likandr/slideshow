package com.homelab.slideshow.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.homelab.slideshow.model.ImageDataItem;
import com.homelab.slideshow.model.ImageDataList;

public class MyUtils {
    final static String LOG_TAG = "Конвертация";

    public static String startData(){
        String[] urls = {
                "",
                "http://fonday.ru/images/tmp/15/4/original/15411ekFAPcyGbfvugQIqmtzMVxojO.jpg",
                "https://wallpaperscraft.ru/image/dodge_charger_srt_hellcat_2015_avto_doroga_97010_720x1280.jpg",
                "https://wallpaperscraft.ru/image/lambordzhini_chernyy_avto_85244_720x1280.jpg",
                "http://wallpapers-images.ru/1280x720/cars/wallpapers/wallpapers-cars-019.jpg",
                "http://wallpapers-images.ru/1280x720/cars/wallpapers/wallpapers-cars-025.jpg"};

        ImageDataList imageData = new ImageDataList();
        for (int i = 0; i < urls.length; i++) {
            ImageDataItem imageDataItem = new ImageDataItem();
            imageDataItem.setId(i);
            imageDataItem.setNumber(i + 1);
            imageDataItem.setUrl(urls[i]);
            imageData.add(imageDataItem);
        }

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String strJson = gson.toJson(imageData);
        Log.i(LOG_TAG, "данные в виде строки");

        return strJson;
    }

    public static ImageDataList stringInModel(String jsonText){

        Gson gson = new GsonBuilder().create();
        ImageDataList imageDataList = gson.fromJson(jsonText, ImageDataList.class);
        Log.i(LOG_TAG, "данные в виде модели");

        return imageDataList;
    }

    public static void showAlertDialog(final Activity context, String message, final boolean finish) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (finish)
                            context.finish();
                    }
                });
        alertDialog.show();
    }
}
