package com.homelab.slideshow.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.homelab.slideshow.R;

public class FavoriteDialogFragment extends DialogFragment implements
        DialogInterface.OnClickListener {
    private View view = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater()
                .inflate(R.layout.dialogfragment_favorite, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return(builder.setTitle("Коментарий:").setView(view)
                .setPositiveButton(android.R.string.yes, this).create());
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        EditText etNote = (EditText)view.findViewById(R.id.et_note_favorite);
        String note = etNote.getText().toString();
        ///
        MainActivity.addNoteAndRefrash(note);
    }

    @Override
    public void onDismiss(DialogInterface unused) {
        super.onDismiss(unused);
    }

    @Override
    public void onCancel(DialogInterface unused) {
        super.onCancel(unused);
    }
}
