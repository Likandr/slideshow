package com.homelab.slideshow.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.homelab.slideshow.utilities.CheckNetworkConnection;
import com.homelab.slideshow.utilities.ImageDownloader;
import com.homelab.slideshow.R;
import com.homelab.slideshow.utilities.MyUtils;

public class PageFragment extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    int pageNumber;

    public static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, null);

        MainActivity.favoritChangeState(MainActivity.mPosition);

        final ImageView imageView = (ImageView) view.findViewById(R.id.imageDisplay);

        if (CheckNetworkConnection.isConnectionAvailable(getActivity())) {
            ImageDownloader imageDownLoader = new ImageDownloader(getActivity(), imageView);
            imageDownLoader.execute(MainActivity.mCurrentImageDataList.getImageData(pageNumber).getUrl());
        } else {
            String message = getResources().getString(R.string.no_internet_connection);
            MyUtils.showAlertDialog(getActivity(), message, true);
        }

        TextView tvDescr = (TextView) view.findViewById(R.id.descrImage);
        if (MainActivity.mCurrentImageDataList.getImageData(pageNumber).getNote() != null) {
            tvDescr.setVisibility(View.VISIBLE);
            tvDescr.setText(MainActivity.mCurrentImageDataList.getImageData(pageNumber).getNote());
        } else
            tvDescr.setVisibility(View.GONE);

        return view;
    }
}
