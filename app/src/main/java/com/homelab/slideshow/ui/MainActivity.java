package com.homelab.slideshow.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.homelab.slideshow.animation.DepthPageTransformer;
import com.homelab.slideshow.animation.FadePageTransformer;
import com.homelab.slideshow.animation.ZoomOutPageTransformer;
import com.homelab.slideshow.model.ImageDataItem;
import com.homelab.slideshow.model.ImageDataList;
import com.homelab.slideshow.model.Setting;
import com.homelab.slideshow.utilities.MyUtils;
import com.homelab.slideshow.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    final String LOG_TAG = "log";
    final String DIALOG_TAG = "favDialog";
    final String NAME_OF_FILE = "ImageData";
    static int PAGE_COUNT = 0;
    static int mPosition = 1;
    static Timer timer = new Timer();
    boolean isStart = true;

    static ViewPager pager;
    static MyFragmentPagerAdapter pagerAdapter;
    static Menu menu;
    static ImageDataList mCurrentImageDataList;
    ImageDataList mDefaultImageDataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try { startDataWriteInFile(); } catch (IOException e){ e.getMessage(); }
    }

    void fill() {
        PAGE_COUNT = mCurrentImageDataList.length() + 1;
        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.setPageTransformer(true,
                (Setting.anim == 0) ? new DepthPageTransformer() :
                        ((Setting.anim == 1) ? new FadePageTransformer() :
                                new ZoomOutPageTransformer()));

        pager.setCurrentItem(1, false);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mPosition = position;
                Log.d(LOG_TAG, "onPageSelected, position = " + position);

                if (position == 0) {
                    pager.setCurrentItem(mCurrentImageDataList.length() - 1, false);
                    pagerAdapter.notifyDataSetChanged();
                } else if (position == mCurrentImageDataList.length()) {
                    pager.setCurrentItem(1, false);
                    pagerAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        startSlideshow();
    }

    public static void favoritChangeState(int position){
        if (menu != null) {
            boolean urlNotEmpty = !mCurrentImageDataList.getImageData(mPosition).getUrl().equals("");
            menu.getItem(0).setVisible(urlNotEmpty);
            if (urlNotEmpty) {
                int drawable = mCurrentImageDataList.getImageData(position).getIsFavorites() ?
                        R.mipmap.ic_favorite :
                        R.mipmap.ic_favorite_empty;
                menu.getItem(0).setIcon(drawable);
            }
        }
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    void readFile() throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(
                openFileInput(NAME_OF_FILE)));
        String str = "";
        while ((str = br.readLine()) != null) {
            builder.append(str);
        }
        mDefaultImageDataList = MyUtils.stringInModel(builder.toString());
        mCurrentImageDataList = MyUtils.stringInModel(builder.toString());
        Log.d(LOG_TAG, "Файл прочитан");

        RefreshView();
    }

    void startDataWriteInFile() throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                openFileOutput(NAME_OF_FILE, MODE_PRIVATE)));
        bw.write(MyUtils.startData());
        bw.close();
        Log.d(LOG_TAG, "Стартовый файл записан");
        readFile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miFavorites:
                onFavoritesClick(item);
                return true;
            case R.id.miSettings:
                onSettingsClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onSettingsClick(){
        stopSlideshow();
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void onFavoritesClick(MenuItem item){
        stopSlideshow();
        if (mCurrentImageDataList.getImageData(mPosition).getIsFavorites()) {
            item.setIcon(R.mipmap.ic_favorite_empty);
            clearNoteAndRefrash();
        } else {
            item.setIcon(R.mipmap.ic_favorite);
            new FavoriteDialogFragment().show(getSupportFragmentManager(), DIALOG_TAG);
        }
    }

    public static void addNoteAndRefrash(String note) {
        ImageDataItem imageDataItem = mCurrentImageDataList.getImageData(mPosition);
        imageDataItem.setIsFavorites(true);
        if (!note.equals("")) {
            imageDataItem.setNote(note);
            pagerAdapter.notifyDataSetChanged();
        }
    }

    private void clearNoteAndRefrash(){
        ImageDataItem imageDataItem = mCurrentImageDataList.getImageData(mPosition);
        imageDataItem.setIsFavorites(false);
        if (imageDataItem.getNote() != null) {
            imageDataItem.setNote(null);
            pagerAdapter.notifyDataSetChanged();
        }
    }

    public void RefreshView() {
        isStart = false;
        ArrayList<ImageDataItem> currentList = new ArrayList<>();
        ArrayList<ImageDataItem> defaultList = new ArrayList<>();

        for (int i = 1; i < mCurrentImageDataList.length(); i++) {
            currentList.add(mCurrentImageDataList.getImageData(i));
        }
        for (int i = 1; i < mDefaultImageDataList.length(); i++) {
            defaultList.add(mDefaultImageDataList.getImageData(i));
        }

        if (Setting.show == 0)
            currentList = defaultList;
        else {
            ArrayList<ImageDataItem> tempList = new ArrayList<>();
            for (ImageDataItem item : currentList)
                if (item.getIsFavorites())
                    tempList.add(item);
            currentList = tempList;
        }

        if (!currentList.isEmpty())
            if (Setting.sort == 0)
                Collections.sort(currentList, new Comparator<ImageDataItem>() {
                    @Override
                    public int compare(ImageDataItem o1, ImageDataItem o2) {
                        return o1.getNumberString().compareTo(o2.getNumberString());
                    }
                });
            else
                Collections.shuffle(currentList);

        currentList.add(0, mCurrentImageDataList.getImageData(0));
        mCurrentImageDataList.setImageDataList(currentList);
        mPosition = 1;

        if (mCurrentImageDataList.isEmpty()) {
            String message = getResources().getString(R.string.no_data_for_show);
            MyUtils.showAlertDialog(this, message, false);

            currentList.add(0, mCurrentImageDataList.getImageData(0));
            pager.setCurrentItem(1, false);
            pagerAdapter.notifyDataSetChanged();
        }else
            fill();
    }

    public static void stopSlideshow() {
        timer.cancel();
    }

    public void startSlideshow() {
        if ((Setting.sldeshow) && (!mCurrentImageDataList.isEmpty())) {
            timer = new Timer();
            int temp = Setting.interval;
            timer.schedule(new SwitchPageTask(), 1000, temp * 1000);
        }
    }

    class SwitchPageTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (mPosition == mCurrentImageDataList.length()) {
                        pager.setCurrentItem(1, false);
                        pagerAdapter.notifyDataSetChanged();
                    } else {
                        pager.setCurrentItem(mPosition + 1, false);
                        pagerAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopSlideshow();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (!isStart)
            RefreshView();
    }
}
