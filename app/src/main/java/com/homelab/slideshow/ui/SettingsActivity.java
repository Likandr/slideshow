package com.homelab.slideshow.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.homelab.slideshow.R;
import com.homelab.slideshow.animation.DepthPageTransformer;
import com.homelab.slideshow.animation.FadePageTransformer;
import com.homelab.slideshow.animation.ZoomOutPageTransformer;
import com.homelab.slideshow.model.Setting;

import java.lang.reflect.Constructor;

public class SettingsActivity extends Activity {
    String[] mInterval = {"5", "10", "15"};
    String[] mShow = {"Всё", "Только избранное"};
    String[] mSort = {"По номеру", "Случайно"};
    String[] mAnim = {"С глубины", "Увядающий", "Уменьшающий"};

    Switch sSlideshow;
    Spinner spInterval, spShow, spSort, spAnim;
    TextView tvInterval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initViewComponents();
    }

    private void initViewComponents(){
        sSlideshow = (Switch) findViewById(R.id.sw_slideshow);
        tvInterval = (TextView) findViewById(R.id.tv_interval);
        spInterval = (Spinner) findViewById(R.id.sp_interval);
        spShow = (Spinner) findViewById(R.id.sp_show);
        spSort = (Spinner) findViewById(R.id.sp_sort);
        spAnim = (Spinner) findViewById(R.id.sp_anim);

        spInterval.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mInterval));
        spShow.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mShow));
        spSort.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mSort));
        spAnim.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mAnim));

        sSlideshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSlideshow()) {
                    sSlideshow.setChecked(true);
                    tvInterval.setEnabled(true);
                    spInterval.setEnabled(true);
                } else {
                    sSlideshow.setChecked(false);
                    tvInterval.setEnabled(false);
                    spInterval.setEnabled(false);
                }
            }
        });
        fillViewComponents();
    }

    private void fillViewComponents() {
        sSlideshow.setChecked(Setting.sldeshow);
        tvInterval.setEnabled(Setting.sldeshow);
        spInterval.setEnabled(Setting.sldeshow);

        spInterval.setSelection((Setting.interval == 5) ? 0 :
                ((Setting.interval == 10) ? 1 : 2));

        spShow.setSelection(Setting.show);
        spSort.setSelection(Setting.sort);
        spAnim.setSelection(Setting.anim);
    }

    public void onClickOk(View v) {
        Setting.sldeshow = isSlideshow();

        Setting.interval = (spInterval.getSelectedItemPosition() == 0) ? 5 :
                ((spInterval.getSelectedItemPosition() == 1) ? 10 : 15);

        Setting.show = spShow.getSelectedItemPosition();
        Setting.sort = spSort.getSelectedItemPosition();
        Setting.anim = spAnim.getSelectedItemPosition();

        finish();
    }

    public void onClickCancel(View v) {
        finish();
    }

    private boolean isSlideshow(){
        return sSlideshow.isChecked();
    }
}
